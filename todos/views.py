from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todolist_list(request):
    todolist_list = TodoList.objects.all()
    context = {
        "todolist_list": todolist_list,
    }
    return render(request, "todos/list.html", context)


def show_todoitem(request, id):
    show_todoitem = TodoList.objects.get(id=id)
    context = {
        "show_todoitem": show_todoitem,
    }
    return render(request, "todos/detail.html", context)


def todolist_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist_list = form.save()
            return redirect("show_todoitem", id=todolist_list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todolist_update(request, id):
    show_todoitem = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            show_todoitem = form.save()
            return redirect("show_todoitem", id=show_todoitem.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todolist_delete(request, id):
    if request.method == "POST":
        todolist_list = TodoList.objects.get(id=id)
        todolist_list.delete()
        return redirect("todolist_list")
    return render(request, "todos/delete.html")


def todoitem_create(request, id):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("show_todoitem", id=todoitem.todolist_list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todoitem_update(request, id):
    show_todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            show_todoitem = form.save()
            return redirect("show_todoitem", id=show_todoitem.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
