from django.urls import path
from todos.views import (
    todolist_list,
    show_todoitem,
    todolist_create,
    todolist_update,
    todolist_delete,
    todoitem_create,
    todoitem_update,
)


urlpatterns = [
    path("todo/", todolist_list, name="todolist_list"),
    path("todo/<int:id>/", show_todoitem, name="show_todoitem"),
    path("todo/create/", todolist_create, name="todolist_create"),
    path("todo/<int:id>/update/", todolist_update, name="todolist_update"),
    path("todo/<int:id>/delete/", todolist_delete, name="todolist_delete"),
    path("todo/<int:id>/create/", todoitem_create, name="todoitem_create"),
    path("todo/<int:id>/update_item/", todoitem_update, name="todoitem_update"),
]
